import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

/// Represents Homepage for Navigation

/// Represents Homepage for Navigation
class Forestal extends StatefulWidget {
  @override
  _ForestalPage createState() => _ForestalPage();
}

class _ForestalPage extends State<Forestal> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Forestal'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            onPressed: () {
              _pdfViewerKey.currentState?.openBookmarkView();
            },
          ),
        ],
      ),
      body: SfPdfViewer.network(
        'https://drive.google.com/uc?export=view&id=1GFojWEt6gwFSjQVYsyKGfHd8OL0uPTxd',
        key: _pdfViewerKey,
      ),
    );
  }
}
