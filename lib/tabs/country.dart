import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MaterialApp(
      home: (Countrytabs()),
    ));

class Countrytabs extends StatefulWidget {
  @override
  _CountrytabsState createState() => _CountrytabsState();
}

class _CountrytabsState extends State<Countrytabs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.green[100],

      body: Container(
        child: ListView(
          children: [
            new Padding(padding: new EdgeInsets.all(10.00)),
            ListTile(
              title: Text(
                "Mensaje",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 23, color: Colors.black),
              ),
            ),
            ListTile(
              subtitle: RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic),
                    children: [
                      TextSpan(
                          text:
                              '"En estos tiempos dificiles, la tecnologia juega un papel tan importante que incluso es la unica manera para que el mundo no se detenga, la educacion no es una excepción. Es por eso que la tematica de esta App o más bien el objetivo, es el dar a conocer el Tecnologico De Chiná pero de una forma distinta en la cual los aspirantes y sociedad en general puedan dar un vistazo a las bastas areas que el instituto ofrece, todo esto pensando en las muchas dificultades de movilización que existen en nuestro país"'),
                    ]),
              ),
            ),
            new Padding(padding: new EdgeInsets.all(10.00)),
            Column(
              children: [
                ListTile(
                  title: Text(
                    'Medios de contacto',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 23, color: Colors.black),
                  ),
                ),
                ListTile(
                  // leading: Icon(Icons.photo_album, color: Colors.blue),

                  title: Text(
                    "Dirección",
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),

                  subtitle: RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                        style: TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                              text:
                                  'Calle 11 S/N ENTRE 22 Y 28, CHINÁ, CAM. MÉXICO. C.P. 24520'),
                        ]),
                  ),
                ),
                ListTile(
                  // leading: Icon(Icons.photo_album, color: Colors.blue),
                  title: Text(
                    "Correo Electronico y Numeros Telefonicos",
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),

                  subtitle: RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                        style: TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                              text:
                                  'Email: dir01_china@tecnm.mx \n Teléfonos: (981) 82 7 20 81, 82 y 52 Ext. 101 y 103 '),
                        ]),
                  ),
                ),
                ListTile(
                  leading: IconButton(
                    icon: const Icon(LineIcons.facebookF),
                    onPressed: _launchURL,
                  ),
                  title: Text("Facebook"),
                  subtitle: RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                        style: TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                            text: '@tecnmcampus.china',
                          ),
                        ]),
                  ),
                ),
                ListTile(
                  leading: IconButton(
                    icon: const Icon(LineIcons.facebookF),
                    onPressed: _launchUR,
                  ),
                  title: Text("Facebook"),
                  subtitle: RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                        style: TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                            text: '@DAE.TECNMCHINA',
                          ),
                        ]),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

_launchURL() async {
  const url = 'https://www.facebook.com/tecnmcampus.china';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_launchUR() async {
  const url = 'https://www.facebook.com/DAE.TECNMCHINA';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

/*Widget nombre() {
  return Text("ESTUDIATES GRADUADOS EN TECNM",
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold));
}*/