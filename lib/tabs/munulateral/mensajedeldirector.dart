import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Mensaje extends StatelessWidget {
  const Mensaje({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildWebView(),
    );
  }

  Widget _buildWebView() {
    return WebView(
      javascriptMode: JavascriptMode.unrestricted,
      initialUrl: 'https://mensaje-del-director-f9266.web.app/',
    );
  }
}
