import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Historia extends StatelessWidget {
  const Historia({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildWebView(),
    );
  }

  Widget _buildWebView() {
    return WebView(
      javascriptMode: JavascriptMode.unrestricted,
      initialUrl: 'https://historia-a1827.web.app/',
    );
  }
}
