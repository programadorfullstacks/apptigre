import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_svg/svg.dart';
import 'munulateral/organigrama.dart';
import 'munulateral/mensajedeldirector.dart';
import 'munulateral/historia.dart';
import 'munulateral/directoriomaestros.dart';
import 'munulateral/normateca.dart';

//import 'package:playmedia/personal.dart';

void main() => runApp(FirstTab());

// ignore: must_be_immutable
class FirstTab extends StatelessWidget {
  List<String> images = [
    "https://drive.google.com/uc?export=view&id=1Hn2zgosPTFcac-ybPIw5uNm1-6oMU2fQ",
    "https://drive.google.com/uc?export=view&id=16A2IXnzgWL1B8ce6hjyQO-NgdSem3P_m",
    "https://drive.google.com/uc?export=view&id=1ODpCwzeLW0UVRqqkzbai6YeHIX7WMT_E"
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blue[900],
            elevation: 0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // ignore: missing_required_param
                IconButton(
                  icon: Image.network(
                      "https://drive.google.com/uc?export=view&id=1egwV-HxzGBCXR3j97iBMOtP8ikPheXpF",
                      width: 100),
                ),
                Text(
                  'TECNM CAMPUS CHINÁ',
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
              ],
            ),
          ),
          body: Container(
              margin: EdgeInsets.all(5.0),
              child: ListView(
                children: [
                  _swiper(),
                  SizedBox(
                    height: 20,
                  ),
                  ListTile(
                    // leading: Icon(Icons.photo_album, color: Colors.blue),
                    title: Text(
                      "MISIÓN",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 23, color: Colors.black),
                    ),

                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text:
                                    'Formar profesionales humanistas con pertinencia, portadores de conocimientos vanguardistas y competitivos; emprendedores e innovadores a través de una educación superior científica y tecnológica de calidad.'),
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    // leading: Icon(Icons.photo_album, color: Colors.blue),
                    title: Text(
                      "VISIÓN",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 23, color: Colors.black),
                    ),

                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text:
                                    'Ser una Institución educativa formadora de ciudadanos del mundo a través del desarrollo sostenido sustentable y equitativo. Con personal capacitado con altos estándares de calidad; protectora del medio ambiente e interactuando con las necesidades que exigen los cambios del País y la comunidad.'),
                          ]),
                    ),
                  ),
                  new Padding(padding: new EdgeInsets.all(4.00)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    height: MediaQuery.of(context).size.height - 500,
                    child: Column(children: [
                      mainCard(context),
                      SizedBox(height: 20),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              child: regularCard('assets/mensaj.svg',
                                  'Mensaje\n del director'),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Mensaje()),
                                );
                              },
                            ),
                            GestureDetector(
                              child:
                                  regularCard('assets/history.svg', 'Historia'),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Historia()),
                                );
                              },
                            ),
                            GestureDetector(
                              child: regularCard(
                                  'assets/directory.svg', 'Directorio'),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Directorio()),
                                );
                              },
                            ),
                            GestureDetector(
                              child: regularCard(
                                  'assets/organigramamas.svg', 'Organigrama'),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PDF()),
                                );
                              },
                            ),
                          ]),
                      SizedBox(height: 20),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              child: regularCard(
                                  'assets/normateca.svg', 'Normateca'),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Normateca()),
                                );
                              },
                            ),
                          ]),
                    ]),
                  ),
                ],
              ))),
    );
  }

// este pedaso de codigo es del carrucel
  Widget _swiper() {
    return Container(
      width: double.infinity,
      height: 150.0,
      child: Swiper(
        viewportFraction: 0.9,
        scale: 0.9,
        itemBuilder: (BuildContext context, int index) {
          return new Image.network(
            images[index],
            fit: BoxFit.fill,
          );
        },
        itemCount: 3,
        control: new SwiperControl(color: Colors.blue[900]),
      ),
    );
  }

// esta parte de codigo son de los iconos de mensaje  historia etc//
  SizedBox regularCard(String iconName, String cardLabel) {
    return SizedBox(
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.blue[900],
          ),
          child: SvgPicture.asset(iconName,
              width:
                  40), //se utiliza puras imagenes svg y que esten guardatos en la carpeta no he buscado como traerlo de la web
        ),
        SizedBox(height: 5),
        Text(cardLabel,
            textAlign: TextAlign.center,
            style: textStyle(13, FontWeight.w500, Colors.blue))
      ]),
    );
  }

  Container mainCard(context) {
    return Container(child: Row(children: []));
  }

  TextStyle textStyle(double size, FontWeight fontWeight, Color colorName) =>
      TextStyle(
        color: colorName,
        fontSize: size,
        fontWeight: fontWeight,
      );
}
